// !Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// !це технологія основний принцип якої заснований на фоновому обміні даними з сервером, при цьому сторінка не перезавантажується повністью, що забезбечує швидку роботу та зручність користування веб сторінкою/додатком. 

class FilmsListCreator {
  request(url) {
    return fetch(url).then((response) => response.json());
  }
  renderList(array) {
    const newList = document.createElement("ul");
    array.forEach((elem) => {
      newList.insertAdjacentHTML("beforeend", `<li>${elem}</li>`);
    });
    return newList;
  }
  loadingAnimationToggle(parentNode, remove) {
    if (remove) {
      parentNode.querySelector(".loading").remove();
    } else {
      parentNode.insertAdjacentHTML(
        "beforeend",
        "<div class='loading'><div class='loading-animation'></div> Loading...</div>"
      );
    }
  }
}

const filmsList = new FilmsListCreator();
const URL = "https://ajax.test-danit.com/api/swapi/films";

filmsList
  .request(URL)
  .then((filmArray) => {
    //! отримуємо масив фільмів, витягуємо з нього необхідну інформацію у вигляді массиву HTML стрічок які будуть всередині "li", створюємо список за допомогою функції "renderList" та додаємо на сторінку.
    document.body.append(
      filmsList.renderList(
        filmArray.map((film) => {
          return `<b>Episode ${film.episodeId}: ${film.name}</b> <br>${film.openingCrawl}<br> Characters:`;
        })
      )
    );
    //! додаємо анімацію завантаження в кінці кожного елементу списку
    document.querySelectorAll("li").forEach((elem) => {
      filmsList.loadingAnimationToggle(elem, false);
    });

    //! повертаємо отриманий масив фільмів щоб обробити його далі
    return filmArray;
  })
  .then((filmArray) => {
    filmArray.forEach((film, index) => {
      //! отримуємо масив промісів для персонажів
      const charactersPromises = film.characters.map((character) => {
        return filmsList.request(character).then((char) => char.name);
      });
      //! після resolve усіх промісів отримуємо масив об'єктів зі значеннями кожного з промісів. перетворюємо його на масив імен персонажів (value)
      Promise.allSettled(charactersPromises)
        .then((resolve) => {
          return resolve.map((elem) => elem.value);
        })
        .then((charArr) => {
          //! створюємо список персонажів  та додаємо його всередину кожного елементу списку фільмів, перед цим видаляючи анімацію завантаження.
          const list = document.querySelector("ul");
          filmsList.loadingAnimationToggle(list.children[index], true);
          list.children[index].append(filmsList.renderList(charArr));
        });
    });
  });
